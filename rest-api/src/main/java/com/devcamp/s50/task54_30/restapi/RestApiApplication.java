package com.devcamp.s50.task54_30.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
		CRandomNumber number = new CRandomNumber();
		System.out.println(number.randomDoubleNumber());
		System.out.println(number.randomIntNumber());
	}

}
