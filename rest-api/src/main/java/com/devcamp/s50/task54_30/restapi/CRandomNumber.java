package com.devcamp.s50.task54_30.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CRandomNumber {
    @GetMapping("/devcap-welcome/random-number")
    public String randomDoubleNumber() {
        double randomDoubleNum = 0;
        for (int i = 0; i < 100; i++) {
            randomDoubleNum = Math.random() * 100;
        }
        return "Random double number from 1 to 100: " + randomDoubleNum;
    }
    @GetMapping("/devcap-welcome/random-int")
    public String randomIntNumber() {
        int randomIntNum = (int) (1 + (Math.random() * 10));
        return "Random in number from 1 to 10: " + randomIntNum;
    }
}
